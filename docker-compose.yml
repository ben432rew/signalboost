version: '3'

networks:
  proxy-tier:

volumes:
  certs:
  html:
  nextcloud:
  postgres_data:
  signal_sock:
  signal_data:
  vhost.d:

services:

  db:
    image: postgres
    container_name: signalboost_db
    volumes:
      - postgres_data:/var/lib/postgresql/data
      - ./backups:/backups
    # as per https://github.com/docker-library/postgres/issues/692
    environment:
      POSTGRES_HOST_AUTH_METHOD: trust
    restart: unless-stopped

  signald:
    image: signald
    container_name: signalboost_signald
    networks:
      - default
    volumes:
      - ./bin:/signalboost/bin
      - signal_data:/var/lib/signald/data
      - signal_sock:/var/run/signald/
      - ./backups:/backups
    restart: unless-stopped

  app:
    image: signalboost
    container_name: signalboost_app
    depends_on:
      - db
      - signald
    entrypoint: /signalboost/bin/entrypoint/app
    env_file: .env
    environment:
      PROJECT_ROOT: "$PWD"
      DEFAULT_LANGUAGE: "${DEFAULT_LANGUAGE:-EN}"
      NODE_ENV: production
    ports:
      - 3000:3000
    networks:
      - proxy-tier
      - default
    volumes:
      - ./.sequelizerc:/signalboost/.sequelizerc
      - ./app:/signalboost/app
      - ./bin:/signalboost/bin
      - ./node_modules:/signalboost/node_modules
      - ./package.json:/signalboost/package.json
      - signal_data:/var/lib/signald/data
      - signal_sock:/var/run/signald/
      - ./yarn.lock:/signalboost/yarn.lock
    restart: unless-stopped

  proxy:
    image: jwilder/nginx-proxy:alpine
    container_name: signalboost_proxy
    ports:
      - 80:80
      - 443:443
    labels:
      com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy: "true"
    volumes:
      - certs:/etc/nginx/certs:ro
      - vhost.d:/etc/nginx/vhost.d
      - html:/usr/share/nginx/html
      - /var/run/docker.sock:/tmp/docker.sock:ro
    networks:
      - proxy-tier
    restart: unless-stopped

  letsencrypt:
    image: jrcs/letsencrypt-nginx-proxy-companion
    container_name: signalboost_letsencrypt
    restart: unless-stopped
    volumes:
      - certs:/etc/nginx/certs
      - vhost.d:/etc/nginx/vhost.d
      - html:/usr/share/nginx/html
      - /var/run/docker.sock:/var/run/docker.sock:ro
    networks:
      - proxy-tier
    depends_on:
      - proxy
